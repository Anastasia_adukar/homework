function conclusion ()
{
	let name = document.getElementById('name').value;
	let text = document.getElementById('text').value;


	let two = document.getElementById('two');
	let div = document.createElement('div');
	let a = two.getElementsByTagName('a')[0];
    let p = document.createElement('p');
    let pTwo = document.createElement('p');
    p.className = 'name';
    p.textContent = name;
    pTwo.className = 'text';
    pTwo.textContent = text;
	div.className = 'card';
	div.appendChild(p);
	div.appendChild(pTwo);
	div.setAttribute('onmousedown', 'card()'); 
	div.setAttribute('draggable', true);
	two.insertBefore(div,a);
}

function card()
{
	let block = document.querySelectorAll('.block');
	let card = document.querySelectorAll('.card');
	let draggedItem = '';

	for (let i = 0; i < card.length; i++)
	{
		let num = card[i];
		num.addEventListener('dragstart', function () {
		draggedItem = num;
		});

		num.addEventListener('dragend', function () {
		setTimeout(function () {
			draggedItem = '';
		}, 0);
		})

		let myblock = document.querySelectorAll('.card');
		myblock.forEach(block => block.addEventListener('dblclick', removeBlock));
		function removeBlock()
		{
			let block = this; 
			block.remove();
		
		}

		for (let j = 0; j < block.length; j++)
		{
			let num1 = block[j];
			num1.addEventListener('dragover', function (e) {
			e.preventDefault();
			});
		
			num1.addEventListener('dragenter', function (e) {
			e.preventDefault();
			this.style.backgroundColor = 'rgba(0, 0, 0, 0.2)';
			});

			num1.addEventListener('dragleave', function (e) {
			this.style.backgroundColor = '#232625';
			});

			num1.addEventListener('drop', function (e) {
			console.log('drop');
			this.append(draggedItem);
			this.style.backgroundColor = '#232625';

			});
		}
		
	}
}

