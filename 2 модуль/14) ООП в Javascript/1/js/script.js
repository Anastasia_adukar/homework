/*1.Реализуйте в прототипном стиле класс Machine с базовыми методами включить/выключить. 
Создайте класс Fridge и отнаследуйтесь от Machine, создайте метод на охлаждение, а 
так же сделайте так, чтобы методы включения/выключения  работали с выводом в консоль 
сообщения с текущим значением enabled.*/
function Machine(on, off) {
	this.on = on;
	this.off = off;
}

Machine.prototype.turnOn = function() 
{
	console.log('Вкл' + this.on);
}

Machine.prototype.switchOff = function()
{
	console.log('Выкл' + this.off);
}

function Fridge (turnOn, switchOff, cooling) 
{
	Machine.call(this, turnOn, switchOff);
	this.cooling = cooling;
}

Fridge.prototype.cooling = function() 
{
	console.log(this.cooling);
}

Fridge.prototype = Object.create(Machine.prototype);
Fridge.prototype.constructor = Fridge;

Fridge.prototype.rrr = function()
{
	Machine.prototype.turnOn.call(this);

}

let result = new Fridge(' охлаждается');
/*result.rrr();*/

/*2.Реализуйте класс MyString, который будет иметь следующие методы: метод reverse(), 
который параметром принимает строку, а возвращает ее в перевернутом виде, метод ucFirst(), 
который параметром принимает строку, а возвращает эту же строку, сделав ее первую букву 
заглавной и метод ucWords, который принимает строку и делает заглавной первую букву 
каждого слова этой строки.*/
class MyString {

	constructor(str)
	{
		this.str = str;
	}

	reverse(str)
	{
		let m = str.split('');
		let arr = m.reverse().join('');
		return arr;
	}
	ucFirst(str)
	{
		return str[0].toUpperCase() + str.slice(1);
	}
	ucWords(str)
	{
		let result;
		let words = str.split(' ');
		for (let i = 0; i < words.length; i++) 
		{
			words[i] = words[i].slice(0, 1).toUpperCase() + words[i].slice(1);
		}
		result = words.join(' ');
		return result;
	}
}

let num = new MyString();
/*console.log(num.reverse('12345678'));
console.log(num.ucFirst('assdf'));
console.log(num.ucWords('gg ij gvghv'));*/

/*3.Реализуйте класс Validator, который будет проверять строки. К примеру, у него будет метод 
isEmail параметром принимает строку и проверяет, является ли она корректным емейлом или нет. 
Если является - возвращает true, если не является - то false. Кроме того, класс будет иметь 
следующие методы: метод isDomain для проверки домена, метод isDate для проверки даты и метод 
isPhone для проверки телефона.*/

class Validator
{
	constructor(email, domain, date, phone)
	{
		this.email = email;
		this.domain = domain;
		this.date = date;
		this.phone = phone;
	}

	isEmail(email)
	{
		let reg = /^[^!@#$%^&*]+[^\.]+@[^\.]+\.[a-zA-Z]{2,6}$/;
		let fun = () => (reg.test(email)) ? true : false;
		console.log(fun());
	}

	isDomain(domain)
	{
		let reg = /^[^!@#$%^&*](\w+\.)+\w/g;
		let fun = () => (reg.test(domain)) ? true : false;
		console.log(fun());
	}
	isDate(date)
	{
		let reg = /(([012]\d|[3][01])\.([0][1-9]|[1][0-2])\.(\d{4}))/;
		let fun = () => (reg.test(date)) ? true : false;
		console.log(fun());
	}
	isPhone(phone)
	{
		let reg = /^((\+375)|(375)|(80))((25)|(29)|(33)|(44))\d{7}$/;
		let fun = () => (reg.test(phone)) ? true : false;
		console.log(fun());
	}
}

let validator = new Validator();
/*validator.isEmail('aaa@gmail.com');
validator.isDomain('aaa.com');
validator.isDate('01.08.2021');
validator.isPhone('+375290000000');*/


/*4.Реализуйте класс Worker (Работник), который будет иметь следующие свойства: name (имя), 
surname (фамилия), rate (ставка за день работы), days (количество отработанных дней). Также 
класс должен иметь метод getSalary(), который будет выводить зарплату работника. Зарплата - 
это произведение (умножение) ставки rate на количество отработанных дней days.*/
class Worker
{
	name = 'Иван';
	surname = 'Иванов';
	rate = 13;
	days = 20;
	getSalary()
	{
		return console.log(this.rate * this.days);
	}
}

new Worker().getSalary();

/*5.Реализуйте класс User принимающий объект со свойствами name и password, предусмотрите метод 
login, возвращающее значение true в случае совпадение связки имя/пароль  и changeName(). 
Отнаследуйте класс User в класс Admin и сделайте так, чтобы админ всегда имел дефолтное поле имя 
(admin) и чтобы в реализации отсутствовала возможность смены имени, а также при логине в консоль
выводилось сообщение о том, что админ залогинен.*/
class User
{
	name1 = "Иван";
	password1 = '111';
	constructor(name, password)
	{
		this.name = name;
		this.password = password;
		
	}
	login(name, password)
	{
		if (this.name1 == name && this.password1 == password)
			return true;
		else
			return false;
	}
	changeName(name)
	{

		return this.name1 = name;
	}

}
class Admin extends User
 {
 	passwordAdmin = "2222";
 	constructor(name, password)
 	{
 		super(name, password);
 	}

 	get name()
 	{
 		return this._name;
 	}
 	set name(value)
 	{
 		this._name = 'Admin';
 	}

 	login(password)
 	{
 		let fun = () => (this.passwordAdmin == password) ? true : false;
 		return fun(); 	}
 }

let obj = 
{
	name: 'Иван',
	password: '111', 
	get full() {
    return `${this.name} ${this.password}`;
  },
  	set full(value) {
    [this.name, this.password] = value.split(" ");
  }
}

let objAdmin =
{
	password: '2222',
	get pass() {
    return `${this.password}`;
  },
  	set pass(value) {
    this.password = value;
  }
}


let user = new User();
/*console.log(user.login(obj.name, obj.password));
console.log(user.changeName('Ваня'));*/

let admin = new Admin();
/*console.log(admin.name);
console.log(admin.login(objAdmin.password));*/




