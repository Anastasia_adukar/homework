/*1) Создайте объект obj = {a: 1, b: 2, c: 3}. Выведите в консоль элемент с 
ключом 'c' двумя способами: через квадратные скобки и через точку. 
Затем выведите все свойства объекта через цикл for..in.*/
function task1()
{
	let obj = {
		a: 1,
		b: 2,
		c: 3,
	};
	let num1, num2;
	let num4 = [];
	num1 = obj["c"];
	num2 = obj.c;
	for (let num3 in obj)
	{
		num4[num3] =  obj[num3];
	}
	return {num1, num2, num4};
	
}
/*console.log(task1());*/

/*2) Создайте объект city, добавьте ему свойства name 
(название города, строка) и population (население, число).*/
function task2()
{
	let city =
	{
		name: "Минск",
		population: 2009786,
	};
	
}

/*3)Создайте массив из шести объектов такого же вида, как city из задачи 2 
– по одному для каждого областного города Беларуси.*/

	var arr = [
		{name: "Минск", population: 2009786},
		{name: "Могилев", population: 357100},
		{name: "Витебск", population: 364800},
		{name: "Гомель", population: 510300},
		{name: "Гродно", population: 356900},
		{name: "Брест", population: 340318},
	];

/*4)Напишите функцию, которая принимает массив из задачи 3 в качестве
 параметра и выводит в консоль информацию о каждом городе.*/
function task4(param)
{
	for (let num in param)
	{
		 console.log(num, param[num]);
	}
}
/*task4(arr);*/

/*5)Создайте в объектах с городами из задачи 3 метод getInfo, который возвращает 
строку с информацией о городе (например, в таком формате: "Город Добруш, население – 18760 человек").*/
function task5(param)
{
	param.getInfo = function()
	{
		 result = param[0];
		 console.log("Город "  + result.name, "население - " + result.population + " человек" );
	};
	param.getInfo();
}
/*task5(arr);*/


/*6)Создайте объект с информацией о себе (имя, фамилия, любимое занятие). 
Добавьте в него метод, который выводит эту информацию в консоль в удобочитаемом формате.*/
function task6()
{
	let obj = 
	{
		name:"Настя",
		surname:"Барсукова",
		hobby:"хх",
		result: function()
		{
			console.log("Имя: " + obj.name +'\n'+ "Фамилия: " + obj.surname + '\n' + "Любимое занятие: " + obj.hobby);
		}
	};

	obj.result();
}
/*task6();*/


/*7) Создайте объект "Цилиндр" (свойства – радиус и высота). 
Добавьте в него метод, который считает объём цилиндра (используя this).*/
function task7()
{
	let pi = 3.14;
	let obj = {
		r: 10,
		h: 20,
	};
	obj.V = function()
	{
		let result = pi * this.r * this.r * this.h
		console.log(result);
	}

	obj.V();
}

/*task7();*/

/*8) Выберите пингвина из списка вымышленных пингвинов на Википедии и опишите 
его в виде объекта (не менее трёх полей; например, имя, создатель и источник).
 Добавьте этому объекту свойство canFly. Добавьте два метода: sayHello, который 
 выводит в консоль приветствие и представление вашего пингвина, и fly, который 
 в зависимости от значения свойства canFly (true или false) определяет, может 
 ли пингвин летать, и сообщает об этом в консоль.*/

 function task8()
 {
 	let penguin = 
 	{
 		name: "Eclair Ecleir Eicler",
 		author: "Kugane Maruyama" , 
 		origin: "Overlord" ,
 		canFly: true,
 	};

 	penguin.sayHello = function()
 	{
 		console.log("Меня зовут: " + this.name);
 	}

 	penguin.fly = function()
 	{
 		if (this.canFly == true)
 		{
 			console.log("Умею летать");
 		}
 		else
 		{
 			console.log("Не летаю");
 		}
 	}

 	penguin.sayHello();
 	penguin.fly();
 }
/* task8();*/

/*9)Создайте несколько (3-4) объектов одинаковой структуры, которые описывают ваши
любимые книги (автор, название, год издания + любые другие свойства на ваше усмотрение). 
Добавьте в каждый из них метод для вывода в консоль краткой информации о книге (например,
в формате Автор — Название).*/
var books = [ 
	    	{author:"Дэниел Киз", name:"Цветы для Элджернона", year: 1966},
			{author:"Рэй Брэдбери", name:"451 градус по Фаренгейту", year: 1953},
			{author:"Джейн Остин", name:"Гордость и предубеждение" , year: 1813},
			{author:"Стендаль", name:"Красное и чёрное", year: 1827},
	];

function task9(param)
{
	param.forEach( book => {
	  console.log("Автор: " + book.author + "Название: " + book.name);
	});
}

task9(books);

/*10)Создайте функцию, которая получает в качестве аргументов два объекта с книгами (из задачи 3)
 и возвращает ту книгу, которая издана раньше.*/

 function task10(param1, param2)
 {
 	if (param1.year > param2.year)
 	{
 		return param2.name;
 	}
 	else 
 		return param1.name;
 }
console.log(task10(books[0], books[1]));