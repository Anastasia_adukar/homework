/*1.Выведите координаты мыши относительно блока в момент движения курсора мыши внутри блока. 
Координаты выводить под блоком в элементе output.*/

document.getElementById('status').onmousemove = () =>
{
 	document.querySelector('.x').innerHTML = 'x: ' + event.offsetX;
    document.querySelector('.y').innerHTML = 'y: ' + event.offsetY;
}

/*2.Реализуйте в верхней части страницы «полоску прогресса», которая будет показывать, на какой 
процент проскроллена страница.*/
let progress = document.querySelector('.progress');
window.addEventListener('scroll', scrollBar);

function scrollBar() {
	let scrollHeight = Math.max(
		  document.body.scrollHeight, document.documentElement.scrollHeight,
		  document.body.offsetHeight, document.documentElement.offsetHeight,
		  document.body.clientHeight, document.documentElement.clientHeight
	);

	let windowHeight = window.pageYOffset;
	let result = windowHeight / (scrollHeight - document.documentElement.clientHeight)* 100;
	progress.style.width = result + '%';
}