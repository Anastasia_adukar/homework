function program(){
	var a = parseInt(document.getElementById('a').value);
	var b = parseInt(document.getElementById('b').value);
	var c = parseInt(document.getElementById('c').value);
	var res = task1(a, b, c);
	alert(res);

}

/*1)Создайте функцию, которая принимает три числа: два первых должны 
быть длиной сторон катетов прямоугольного треугольника, а третье – 
длиной гипотенузы. Функция возвращает true, если такой прямоугольный 
треугольник возможен, и false, если нет.*/
function task1(a,b,c)
{
	if (a**2 + b**2 == c**2)
	{
		return true;
	}
	else
		return false;
}

/*2)Создайте функцию, которая принимает два числа, и определяет, делится
 ли одно из них на другое без остатка. Из функции верните true или false.
*/

function task2(a, b)
{
	if (a % b == 0)
	{
		
		return alert(true);
	}
	else 
		return alert(false);

}

/*3)Создайте функцию, которая получает в качестве аргумента оценку по 10-балльной
шкале. Для ошибочных аргументов (0, «B») функция возвращает текстовую ошибку, 
для верных – их текстовое описание из Википедии.*/
function task3(num7)
{
	if (num7 < 0 || num7 > 10 )
	{
		alert("Таких отметок нет");
	}
	switch(num7)
	{
		case '1':
		alert('Unsatisfactory');
		break;
		case '2':
		alert("Unsatisfactory");
		break;
		case '3':
		alert("Unsatisfactory");
		break;
		case '4':
		alert("Unsatisfactory");
		break;
		case '5':
		alert("Almost good");
		break;
		case '6':
		alert("Good");
		break;
		case '7':
		alert("Very good");
		break;
		case '8':
		alert("Almost excellent");
		break;
		case '9':
		alert("Excellent");
		break;
		case '10':
		alert("Brilliant");
		break;
	}
}


/*4)Создайте функцию, которая принимает в качестве аргумента целое число, 
соответствующее порядковому номеру месяца. Если месяц с таким номером 
существует, функция возвращает имя сезона (лето, осень, зима, весна), 
к которому относится месяц. Если нет – сообщение об ошибке.*/
function task4(num)
{
	let obj = 
	{
		a:"Зима",
		b:"Весна",
		c:"Лето",
		d:"Осень",
	}

	if (num == 1 || num == 2 || num == 12)
	{
		return alert(obj.a);
	}
	else if (num >= 3 && num <= 5)
	{
		return alert(obj.b);
	}
	else if (num >= 6 && num <= 8)
	{
		return alert(obj.c);
	}
	else if (num >= 9 && num <= 11)
	{
		return alert(obj.d);
	}
	else
		return alert("Такого месяца нет");

}

/*5)Используя цикл do..while, создайте функцию, которая принимает числа через prompt(), 
пока не будет введено число 0.*/
function task5()
{
	let num;
	let result = [];
	do {
		num = +prompt("Число");
		result += num + ',';

	}
	while (num != 0)
	return alert(result);	
}

/*6)Перепишите функцию из задачи 5 так, чтобы она принимала числа, пока их сумма остаётся 
меньше 100, выводила эту сумму в консоль, а возвращала количество введённых чисел.*/
function task6()
{
	let num;
	let result = 0;
	let i = 0;
	do {
		num = +prompt("Число");
		result += num;
		++i;
	}
	while (result < 100)
	alert(result + '\n' + i);
	
}
/*7)Создайте функцию, которая выводит в консоль числа от 10 до 99, заканчивающиеся на 7 
или делящиеся на 7 (в убывающем порядке).*/
function task7()
{
	let result = [];
	let j = 0;
	for (let i = 10; i <= 99; i++)
	{
		if (i % 10 == 7 || i % 7 == 0)
		{
			result[j] =i;
			j++;
		}
	}
	result.reverse();
	alert(result);
}


/*8)Создайте функцию, которая принимает два аргумента – количество учеников в классе и 
количество парт в кабинете. Функция возвращает строку вида «не хватает 
парт: 2» / «лишних парт: 1». Важно: за одной партой может сидеть два ученика!*/
function task8(p, t)
{
	let result = 2 * t - p;

	if (result == 0)
	{
		return alert("Хватило");
	}
	if(result < 0)
	{
		let num = -1 * (result / 2);
		return alert("Не хватает: " + num);
	}
	if (result > 0)
	{
		let num1 = result / 2;
		return alert("Лишних парт: " + num1);

	}
}


/*9)Создайте функцию words(),  которая в зависимости от переданного в нее целого числа n,
будет выводить слово «карандаш» с правильным окончанием («12 карандашей», но «22 карандаша»).*/
function task9(num9)
{
	switch(num9 % 10)
	{
	   case 1:
	   alert(num9 + " карандаш")
	   break;
       case 2:
       case 3:
       case 4:
       alert(num9 + " карандаша");
       break;
       case 5:
       case 6:
       case 7:
       case 8:
       case 9: 
       alert(num9 + " карандашей");
       break;
       default: 
       alert("Ошибка, введите положительное число!");
       break;
	}

}

/*10)Создайте функцию, которая получает два числа и возвращает знак их произведения. 
Если результат равен нулю, возвращать ‘+’.*/

function task10(num3, num4)
{
	let result = num3 * num4;
	if (result > 0 || result == 0)
	{
		alert("+");
	}
	else
	{
		alert("-");
	}
}

/*11)Создайте функцию, которая получает число и выводит в консоль все его делители.*/
function task11(num5)
{
	let result = [];
	for (let i = 0; i <= num5; i++)
	{
		if (num5 % i == 0)
		{
			result +=  i + " ";

		}
	}
	alert(result);
}
/*12)Создайте функцию, которая проверяет, можно ли представить число в виде суммы 
квадратов двух целых однозначных чисел.*/
function task12(num6)
{
	let m = 0;

	for (let i = 1; i <= num6; i++)
	{
		for (let j = 1; j <= num6; j++)
		{
			if ((i * i + j * j == num6) && i >= 0 && i <= 9 && j >= 0 && j <= 9)
			{
				m++;
				alert("Первое число: "+ i + "\n" + "Второе число: " + j +"\n"+ "Результат: " + num6);
			}
		}
	}

	if (m == 0)
	{
		alert('Таких значений нет');

	}
}
