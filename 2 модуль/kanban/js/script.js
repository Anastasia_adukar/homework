let blockHistory = document.querySelectorAll('.block-history');
let id = 0;

function add()
{
	id = id + 1
	return id;

}

function conclusion()
{
	let name = document.getElementById('name').value;
	let text = document.getElementById('text').value;

	let two = document.getElementById('two');
	var div = document.createElement('div');
	let a = two.getElementsByTagName('a')[0];
    let p = document.createElement('p');
    let pTwo = document.createElement('p');
    


    p.className = 'name';
    p.textContent = name; 
    pTwo.className = 'text';
    pTwo.textContent = text;
	div.className = 'card';
	div.appendChild(p);
	div.appendChild(pTwo);
	div.setAttribute('onmousedown', 'card()'); 
	div.setAttribute('draggable', true);
	div.setAttribute('id', add())

	two.insertBefore(div,a);

	history.putHistory('<br>'+'Добавлено задание.'+'<br>'+ '№' + id +'<br>' +'Имя: '+ p.textContent +'<br>'+ 'Задание: ' + pTwo.textContent + '<br>' + '<br>');
	document.getElementById('history').innerHTML =  history.getHistory().join('');
	
	console.log(history.getHistory());
}



function card()
{
	let block = document.querySelectorAll('.block');
	let card = document.querySelectorAll('.card');
	let div = document.querySelectorAll(".block");
	let draggedItem = '';
	let arr = [];

	for (let i = 0; i < card.length; i++)
	{
		for (let j = 0; j < block.length; j++)
		{
			let num = card[i];
			let num1 = block[j];

			num.ondragstart = function(e)
			{
				draggedItem = num;
				let ID = e.target.id;
				arr += ID;
				/*console.log(arr)
				console.log(ID)*/
			}

			num.ondragend = function()
			{
				setTimeout(function () {
				draggedItem = '';
				}, 0);
			}

			block[1].ondrop = function()
				{
					history.putHistory('Задание №'+ arr +' выполняется' + '<br>' + '<br>');
					document.getElementById('history').innerHTML =  history.getHistory().join('');
					console.log(history.getHistory());

				}
			block[2].ondrop = function()
				{
					history.putHistory('Задание №'+ arr +' выполнено' + '<br>' + '<br>');
					document.getElementById('history').innerHTML =  history.getHistory().join('');
					console.log(history.getHistory());
				}


			num.ondblclick = function(e)
			{
				let idDel = this.id;

				console.log(idDel);
				let block = this;
				block.remove();
				history.putHistory('Карточка №' + idDel + ' удалена ' + '<br>' + '<br>');
				document.getElementById('history').innerHTML = history.getHistory().join('');
				console.log(history.getHistory());
			}
		
			num.addEventListener('mousedown', function()
			{
				num1.style.backgroundColor = 'rgba(0, 0, 0, 0.2)';
			});

			num.addEventListener('mouseup', function()
			{
				num1.style.backgroundColor = '#232625';
			});

			num.addEventListener('mousemove', function()
			{
				num1.style.backgroundColor = '#232625';
			});

			num1.addEventListener('dragover', function (e) {
				e.preventDefault();
			});
		
			num1.addEventListener('dragenter', function (e) {
				e.preventDefault();
				this.style.backgroundColor = 'rgba(0, 0, 0, 0.2)';
			});


			num1.addEventListener('dragleave', function (e) {
				this.style.backgroundColor = '#232625';
			});

			num1.addEventListener('drop', function (e) {
				this.append(draggedItem);
				this.style.backgroundColor = '#232625';
			});
		}
		
	}
}

class History
{
	constructor ()
	{
		this.name = 'key';
	}


	getHistory()
	{
			const historyLocal = localStorage.getItem(this.name);
			if (historyLocal !== null)
			{
				return JSON.parse(historyLocal);
			}
			return [];
	}

	putHistory(id)
	{
		let history = this.getHistory();
		history.push(id);
		localStorage.setItem(this.name, JSON.stringify(history));
	}
}


let history = new History();

localStorage.clear();