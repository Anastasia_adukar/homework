/*1)Напишите функцию counterFactory(start, step), которая при вызове 
возвращает другую функцию – счётчик tictoc(). start – стартовое 
значение счётчика, step – его шаг. При каждом вызове tictoc увеличивает значение счётчика на step.*/
function counterFactory(start, step)
{
	function tictoc()
	{
		start += step;
		return start;
	}

	return tictoc;
}
let result = counterFactory(10, 10);
/*console.log(result());*/


/*2)Напишите функцию take(tictoc, x), которая вызывает функцию tictoc заданное 
число (x) раз и возвращает массив с результатами вызовов.*/
function tictoc(x)
{
	return x;
}
function take(tictoc, x)
{
	let arr = [];
	x = +prompt('Введите х', 10);
	for(let i = 0; i < x; i++)
	{
		arr[i] = tictoc(i);
	}
return arr;
}

/*console.log(take());*/


/*3)Разбейте текст этой задачи на отдельные слова, удаляя по пути точки и запятые, а 
полученные слова сложите в массив. Напишите функцию, которая возвращает массив из 
тех же слов, но развёрнутых задом наперёд, причём массив должен быть отсортирован 
по количеству букв в слове. Напишите другую функцию, которая считает общее количество 
букв «с» во всех элементах массива.*/
function task3()
{
	let str = "Разбейте, текст. этой, задачи. на отдельные слова, удаляя по пути точки и запятые."
	let arr = '';
	result = [];
	arr = str.split('.').join();
	result = arr.split(' ');
	let m = mas(result);
	let letters = countingLetters(str);
	return {result, letters};
}

function mas(result)
{
	for(let i = 0; i < result.length; i++)
	{
		result[i] = result[i].split('').reverse().join('');
		result[i] = result[i].replace(',', '');
	}
	
	result.sort((function(a, b) 
	{ 
		return a.length - b.length; 
	}));
}

function countingLetters(str)
{
	let k = 0;
	for (let i = 0; i < str.length; i++)
	{
		if (str[i] == 'с')
		{
			k++;
		}
	}
	return k;
}
/*console.log(task3());*/



/*4)Напишите функцию, которая принимает в качестве параметра строку из одного символа. 
Подсчитайте количество таких символов во всех элементах массива, как в задаче 3 из практики. 
При подсчёте прописные и строчные буквы считайте одинаковыми.*/
function task4()
{
	let str = prompt("Строка");
	let symbol = prompt("Символ");
	let result = [];
	result = str.split('');

	let result1 = s(result, symbol);

	return { result, result1};
}
function s(result, symbol)
{
	let k = 0;
	for(let i = 0; i < result.length; i++)
	{
		if (result[i] == symbol)
		{
			k++;
		}
	}
	return k;
}
console.log(task4());

/*5)Напишите функцию, которая будет возвращать частичную функцию от функции из задачи 1, фиксируя искомый символ.*/

/*6)Отфильтруйте массив городов так, чтобы в нём остались только города из штата Техас, 
которые с 2000 по 2013 выросли в населении.*/

	let arr = [];
	arr = data;
	let city = [];
	for(var i = 0; i < arr.length; i++) 
	{
 		if(arr[i].state == 'Texas' && parseInt(arr[i].growth_from_2000_to_2013) > 0)
 		{
   			city.push(arr[i]);
  		}
  	}
	/*console.log(city);*/
/*7)Подсчитайте, сколько миллионов населения живёт во всех городах на долготе от -110 до -120 градусов.*/
function task7()
{
	let arr = [];
	arr = data;
	console.log(arr)
	let result = 0;
	for(var i = 0; i < arr.length; i++) 
	{
		if(parseInt(arr[i].longitude) < -110 && parseInt(arr[i].longitude) > -120 )
		{
			result += parseInt(arr[i].population);
		}
	}
	return(result);
}
/*console.log(task7());
*/
/*8)Создайте массив только из тех городов, которые начинаются на букву S, при этом отсортируйте элементы 
этого массива по названию штата.*/
function task8()
{
	let arr = [];
	arr = data;
	arr.sort((a, b) => {
    if ( a.state < b.state ) 
    {
    	return -1;
    }
    if ( a.state < b.state )
    { 
    	return 1;
    }
	});
	let result = [];
	let m = [];
	let s = [];

	for(let i = 0; i < arr.length; i++)
	{
		m = arr[i].city;
		if (m[0] == "S")
		{
			result += m + '\n';
		}
	}
	

	return {arr,result};
}
/*console.log(task8());*/