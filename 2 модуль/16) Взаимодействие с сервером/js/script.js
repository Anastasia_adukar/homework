/*1.Превратите простой объект в JSON*/
let obj = 
{
	name: 'Иван',
	password: 111,
}

let result = JSON.stringify(obj);
/*console.log(result);*/
/*2.Создайте объект со ссылкой на другой объект и сконвертируйте его в JSON. А потом назад*/

let user = 
{
	name: 'Иван',
	age: 30,
}

let user1 = 
{
	name: user,

}

let result2 = JSON.stringify(user1); 
let m = JSON.parse(result2);

/*console.log(result2);
console.log(m)*/


/*3.Используя ссылку получите данные, найдите изображения и вставьте их в документ, также оберните 
их в ссылки, используя свойства link.*/

let url = 'https://www.martinakis.com/prints';
let xhr = new XMLHttpRequest();
let photo = [];
let img1 = [];
xhr.open('GET', url);
xhr.responseType = "document";
let t = [];
xhr.onload = () =>
{
	let script = xhr.response.getElementsByTagName('script')[5];
	photo = JSON.parse(script.text);
	t = photo.itemListElement;
	for (let i = 0; i < t.length; i++)
	{
		img1[i] = t[i].image;
		let div = document.querySelectorAll('div');
		div.forEach(el => el.insertAdjacentHTML('beforeend', '<img src="'+img1[i]+'">'));
	}
		
	console.log(img1);
}

xhr.send();


/*4.Создайте html файл и наполните его произвольными данными. Напишите функцию получения этого файла 
используя синхронный/асинхронный подходы. Напишите обработчики события на каждый тип события. Посмотрите в отладчике, 
что происходит на каждом шагу выполнения запроса.*/
function asynchronous()
{
	let url = 'https://www.martinakis.com/';
	let xhr = new XMLHttpRequest();
	xhr.open('GET', url);
	xhr.responseType = 'text';
	xhr.send();
	xhr.onload = function()
	{
		if(xhr.status != 200)
		{
			console.log("Ошибка");
		}
		else
		{
			console.log(xhr.response);
		}
	}
}

function synchronous()
{
	let url = 'https://www.martinakis.com/';
	let xhr = new XMLHttpRequest();

	xhr.open('GET', url, false);
	try
	{
		xhr.send();
		if(xhr.status != 200)
			console.log('Ошибка');
		else
			console.log(xhr.response);
	}
	catch
	{
		alert("Запрос не удался");
	}
}

asynchronous();
synchronous();

