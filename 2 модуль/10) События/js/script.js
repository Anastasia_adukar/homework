/*1.Сделайте так, чтобы по нажатию на кнопку выводился alert*/
function task1() {
	alert(':)'); 
}
/*2.Создайте кнопку с произвольным текстом, сделайте так, чтобы по нажатию текст изменялся на button.*/
function task2()
{
	let text = 'button';
	document.querySelector('button[name = "task2"]').innerHTML = text;
}
/*3.Создайте форму с полем для ввода и двумя кнопками «enable» и «disable». Сделайте так, 
чтобы по нажатию на кнопку «disable» поле становилось неактивным, на «enable» — активным.*/
function disable()
{
	let b = document.querySelector('input[name ="search"]');
	b.setAttribute("disabled", "disabled");
}
function enable()
{
	document.getElementById("text").disabled = false;
}
/*4.Создайте объект произвольной формы и цвета. Сделайте так, чтобы объект можно было двигать,
а его координаты на странице выводились в HTML-элементе output.*/

let rotateX = 0;
let rotateY = 0;
let left = 0;
let top1 = 0;

document.onkeydown = function (e) {
  switch (e.keyCode) {
    case 87:
      top1 -= 10;
      break;
    case 83:
      top1 += 10;
      break;
    case 65:
      left -= 10;
      break;
    case 68:
      left += 10;
      break;
/*Управление по оси X, Y*/
    case 37:
      rotateY -= 4;
      break;
    case 38:
      rotateX += 4;
      break;
    case 39:
      rotateY += 4;
      break;
    case 40:
      rotateX -= 4;
      break;
  }

  document.getElementById('container').style.transform =`rotateY(${rotateY}deg)` +`rotateX(${rotateX}deg)`;

  document.getElementById('container').style.left = left + 'px';
  document.getElementById('container').style.top = top1 + 'px';

  let container = document.getElementById('container');
  let coord = container.getBoundingClientRect();
  console.log('x:' + coord.left);
  console.log('y:' + coord.top);
};

/*5.Создайте поле ввода, сделайте с помощью JS так, чтобы в него можно было вводить только числовые значения.*/
function task5()
{
	if (event.keyCode < 96)
	{
		if(event.keyCode > 105)
		{
   			event.preventDefault();

		} 
	}
}

/*6.Создайте блок div, в качестве изображения фона установите ему изображение закрытой папки. Добавьте событие,
которое выполняется при двойном клике на блоке и заменяет фон блока на изображение открытой папки.*/

function task6()
{
	document.querySelector('.folder-one').classList.replace('folder-one', 'folder-two');
}


/*7.Добавьте в документ 300-400 блоков div квадратной формы с размерами сторон 30px и цветом фона.
Добавление элементов выполните с помощью цикла, цвет попытайтесь раздать случайным образом 
(это условие выполнять не обязательно). Добавьте событие при наведении мыши, которое будет 
скруглять данные блоки с помощью border-radius до круга. Для замедления эффекта скругления в CSS
 можно добавить transition.*/


/*8.Дан инпут. Дана кнопка. По нажатию на кнопку клонируйте инпут.*/
function task8()
{
    let m = document.querySelector('input[name = "text2"]');
    let el = m.cloneNode();
    document.body.appendChild(el);
}

