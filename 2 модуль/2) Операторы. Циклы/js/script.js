function task1()
{
	let num1 = prompt('Первое число');

	while (!parseInt(num1))
	{
		num1 = prompt('Первое число');
	}

	let num2 = prompt('Второе число');

	while (!parseInt(num2))
	{
		num2 = prompt('Второе число');
	}

	if (num1 > num2)
	{
		console.log(num1);
	}
	else if (num1 == num2)
	{
		console.log('Числа равны');
	}
	else
	{
		console.log(num2);
	}
}

function task2()
{
	let num1 = prompt('Первое число');

	while(!parseInt(num1))
	{
	 num1 = prompt('Первое число');
	}

	let num2 = prompt('Второе число');

	while(!parseInt(num2))
	{
	 num2 = prompt('Второе число');
	}

	let num3 = prompt('Третье число');

	while(!parseInt(num3))
	{
	num3 = prompt('Третье число');
	}

	let arr = [num1, num2, num3];

	arr.sort(function(a,b)
		{
		return a - b;
	});
	console.log(arr[0], arr[1], arr[2]);
}

function task3()
{
	let num1 = prompt('Первое число');

	while (!parseInt(num1))
	{
		num1 = prompt('Первое число');
	}

	let num2 = prompt('Второе число');

	while (!parseInt(num2))
	{
		num1 = prompt('Второе число');
	}

	let result = num1 > num2 ? num1 : num1 == num2 ? 'Числа равны' : num2;
	console.log(result);

}

function task4()
{
	let num = prompt('Введите число ');

	while (!parseInt(num))
	{
		num = prompt('Второе число');
	}

	switch(num)
	{
		case 1:
		console.log('Разные');
		break;
		case 2:
		console.log('Разные');
		break;
		case 3:
		console.log('Разные');
		break;
		default:
		console.log('Одинаковые');
	}
}

function task5()
{
	let x = prompt('Введите число ');

	while (!parseInt(x))
	{
	 x = prompt('Введите число ');
	}
	let sum = 0;
	for (let i = 1; i <= x; i++)
	{
		sum = sum + i;
	}
	console.log (sum);
}

function task6()
{
	let x = prompt('Введите число ');

	while (!parseInt(x))
	{
	 x = prompt('Введите число ');
	}

	for (let i = x; i >= 0; i--)
	{
		if (i % 2 == 0)
		{
			console.log(i);
		}
	}
}

function task7()
{
	let x = prompt('Введите число ');

	while (!parseInt(x))
	{
	 x = prompt('Введите число ');
	}

	let sum = 0, i=0;
	while (i <= x)
	{
		sum = sum + i;
		i++;
	}
	console.log(sum);
}

function task8()
{
	let x = prompt("Введите положительное число ");

	while (!parseInt(x) || x < 0)
	{
		x = prompt('Введите положительное число  ');
	}
	
	function factor(x) {
		if (x == 0)
		{
			return 1;
		}
		
		return factor(x - 1) * x;
	}

	let result = factor(x);
	console.log(result);

}

function task9()
{
	let x = prompt("Введите число ");

	while (!parseInt(x) || x < 0)
	{
		x = prompt('Введите положительное число  ');
	}

	function fib (x) 
	{
		if (x < 2)
		{
			return x;
		}
		return fib(x - 2) + fib(x - 1);
	}

	let result = fib(x);
	console.log(result);
}

function task10()
{
	let a = prompt("Введите год: ");

	while (!parseInt(a))
	{
		a = prompt('Введите год:', 1997);
	}

	if (a % 4 == 0)
	{
		console.log("Год високосный");
	}
	else
	{
		console.log("Год не високосный");
	}
}

function task11()
{
	let num = " ";
	let num1 = "#";
	let result ="";
	for (let i = 0; i < 8; i++) {
		for (let j = 0; j < 8; j++)
			if( j % 2 == 0)
			{
				if (i % 2 == 0)
				{
					result += num;
			    }
				else
				{
				    result += num1;
				}
			}
			else if (i % 2 == 0)
			{
				result += num1;
			}
			else
			{
				result += num;
			}
			result+="\n";
	}
	console.log(result);
}