/*1.Создайте селект с несколькими опциями. Добавьте одну опцию, используя Javascript. 
Сделайте так, чтобы по выбору опции выводилось сообщение с данными этой опции (текст + значение).*/
	let select = document.createElement('select');
	let option = document.createElement('option');
	option.textContent = 'Пункт 3';
	option.setAttribute('value', 3)
	let value4 = document.querySelector('select');
	value4.appendChild(option);

	function task1()
	{
		let select = document.querySelector('select');
		let option = document.querySelector('option');
		let k = select.options[select.selectedIndex];
		console.log('Значение: ' + k.value +'\n' +'Текст: ' + select.options[select.selectedIndex].text);
	}

/*2.Создайте форму вычисления процентов по вкладу.*/
	
	let depositsSum = document.querySelector('input[name = "sum"]');
	let depositsMonth = document.querySelector('input[name = "month"]');
	depositsSum.oninput = function() 
	{
    	document.getElementById('initially').innerHTML = depositsSum.value;
	};

    depositsMonth.onchange = function()
    {
     	let num = document.getElementById('result');
     	let result = (depositsSum.value * depositsMonth.value) / 100;
     	let result1 = result + Number.parseInt(depositsSum.value);
    	num.innerHTML = result1;
    };
/*3.Создайте регулярное выражение для поиска трёх точек.*/
function task3()
{
	let str = document.getElementById('point').value;
	alert(str.match(/\.{3}/g));
}
/*4.Создайте regexp, который ищет все положительные числа, в том числе десятичные.*/
function task4()
{
	let str = document.getElementById("number").value;
	let reg = /[^-0-9\.][0-9\.]{1,}/g;
	alert(str.match(reg));
}

/*5.Создайте регулярку, которая ищет цвета в формате #eee, #eeeddd*/
function task5()
{
	let color = document.getElementById("color").value;
	let reg = /#([0-9a-zA-Z]{3}\b|[0-9a-zA-Z]{6}\b)/g;
	alert(color.match(reg));
}
/*6.Предложите строку, которая подойдет под выражение ^$*/
function task6()
{
	let reg = /^$/;
	let str ='';
	let result = reg.test(str);
	alert(result);
}
/*task6();*/
/*7.Создайте HTML-форму регистрации с полем пароля. По клику на кнопку проведите 
валидацию пароля: он должен содержать хотя бы одну цифру, один спецсимвол и одну 
букву, а так же быть длиннее 6 знаков. При прохождении валидации выводить приветственное 
сообщение, в противном случае - ошибку.*/
function task7()
{
	let str = "";
	let password  = document.getElementById('password').value;
	let reg = /(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[!@#$%^&*]){6,}/g;
	if(reg.test(password))
  	{
    	alert("Верно");
   		str += "Пароль: ";
   		for(let i = 0; i < password.length; i++)
    	{
     		str += "*";
    	}
    	alert(str);
 	}
 	else
 	{
    	alert("Ошибка");
 	}
}

/*8.Напишите функцию, которая получает любую форму в качестве аргумента и проверяет введённые
в форму данные (в input типа email должен быть введён email, в input type="tel" — белорусский 
номер телефона; пароль должен содержать не менее восьми символов, среди которых есть одна маленькая, 
одна большая буква, одна цифра и один спецсимвол; самостоятельно добавьте ещё хотя бы две логичные 
проверки). Все проверки проводить с помощью регулярных выражений. При возникновении ошибок выдавать 
их пользователю рядом с проверенными полями! Если форма заполнена верно — выдайте приветственное сообщение*/
function task8()
{
	let str = '';
	let name = document.getElementById('name').value;
	let email = document.getElementById('email').value;
	let phone = document.getElementById('phone').value;
	let password = document.getElementById('password-form').value;
	let regName = /^([A-Z][a-z]+)[,\s]\s*([A-Z][a-z]+)\s*$/;
	let regEmail = /^[^!@#$%^&*]+[^\.]+@[^\.]+\.[a-zA-Z]{2,6}$/;
	let regPhone = /^(\+375)|(375)(25)|(29)|(33)|(44)\d{7}$/;
	let regPassword = /(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*])(?=.*\d){8,}/g;

	if (regName.test(name))
	{
		str += "Имя и фамилия: " + name + "\n";
	}
	else
	{
		alert("Введите ваше имя.");
	}

	if (regEmail.test(email))
	{
		str += "Email: " + email +'\n';
	}
	else
		alert('Введите правильный Email.');
	if(regPhone.test(phone))
	{
		str += "Телефон: " + phone + '\n';
	}
	else
	{
		alert('Введите номер телефона.')
	}
	if(regPassword.test(password))
	{
		str += "Пароль: " + password + '\n';
	}
	else
	{
		alert('Введите пароль.')
	}
	if(regName.test(name) && regEmail.test(email) && regPhone.test(phone) && regPassword.test(password))
	document.getElementById('reg_dat').innerHTML = "Регистрация прошла успешно\n" + str;
	
}
/*9.Используя регулярные выражения, напишите скрипт, который заменяет слово «функция» в nтексте 
на «function». Если получится, сделайте так, чтобы «функция» заменялась в любом падеже.*/
function task9()
{
	let text = document.getElementById('text').textContent;
	let result = text.replace(/функция/ig, 'function');
	document.getElementById('result-text').innerHTML = result;
}
/*10.С помощью регулярных выражений определите, является ли строка корректным временем вида '09.59 am', '12.30 pm'.*/
function task10()
{
	let str = '12.30 pm';
	let reg = /(([01][0-9])\.[0-5]\d\s[am])|(([1][2-9])|([2][0-3])\.[0-5]\d\s[pm])/;
	if(reg.test(str))
	{
		alert("Верно");
	}
	else
		alert('Ошибка');
}
/*task10();*/
/*11.Удалите одной регуляркой все повторяющиеся слова из строки, например для 'dsf xxx aaa xxx sd aaa' 
должно вернуть 'dsf xxx aaa sd'.*/
function task11()
{
	let str = 'dsf xxx aaa xxx sd aaa';
	let result = str.replace(/\b(\w{1,})\b(?=.*\1)/g,'');
	alert(result); 
}

/*task11();*/
