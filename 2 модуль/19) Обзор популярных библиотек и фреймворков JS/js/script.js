var app = new Vue(
{
	el:"#app",
	data:
	{
		tables: [
		{
			image: 'https://content2.onliner.by/catalog/device/header/5e55ad13b6ea58527cc393ce0111072c.jpeg',
			type:'Стол Тэкс Квартет-2 + стеллаж (белый)',
			description:'стол для работы, прямоугольная форма, 1250x500 мм, высота 1020 мм, материал: ЛДСП (столешница), ЛДСП (опоры)',
		},
		{
			image: 'https://content2.onliner.by/catalog/device/header/79325f283656038b67a189ed84c41e69.jpeg',
			type: 'Геймерский стол Сокол КСТ-18 (белый)',
			description:'геймерский стол, прямоугольная форма, 1350x800 мм, высота 750 мм, материал: ЛДСП (столешница), хромированная сталь/ЛДСП (опоры)',
		}
		],
	}, 
	methods:
	{
		deleteTable(tbl)
		{
			this.tables.splice(tbl, 1);
		},
	}
})