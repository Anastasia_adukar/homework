//Создайте массив и в цикле заполните его чётными числами от 2 до 20.
function task1()
{
	let arr = [];
	let j = 0;
	for (let i = 2 ; i <= 20; i++)
	{
		if (i % 2 == 0)
		{
			arr[j] = i ;
			j++;
		}

	}
	return arr;
}  

/*console.log(task1());*/

//Преобразуйте массив из задачи 1 так, чтобы его элементы стали равны своему индексу, умноженному на 5.
function task2()
{
	let arr = [];
	let a = 20;
	for (let j = 0; j <=9; j++)
	{
			
		arr[j] = j * 5 ;
	}
	return arr;
}
/*console.log(task2());*/

//Преобразуйте массив из задачи 2 так, чтобы его элементы расположились в обратном порядке.
function task3()
{
	let arr = [];
	let result = [];
	for (let i = 2; i <= 20; i++)
		{
			if (i % 2 == 0)
			{
				arr.unshift(i);
			}
		}
	return arr;
}
/*console.log(task3());*/

//Получите от пользователя три числа, создайте из них массив. 
//Используя циклы, найдите наибольшее из чисел и разделите на него каждый элемент массива.
function task4()
{
	let num1 = +prompt("Первое");
	let num2 = +prompt("Второе");
	let num3 = +prompt("Третье");
	let arr = [num1, num2, num3];
	let max = arr[0];
		for (let i = 0; i < arr.length; i++)
		{
			if (max <= arr[i])
			{
				max = arr[i];
			}
		}
		for (let j = 0; j < arr.length; j++)
		{
			arr[j] /= max;

		}
	return {max, arr};
}
/*console.log(task4());*/

function getRandomArray(len)
{
	let arr = [];
	let num = 20;
	for (let i = 0; i <= len-1; i++)
	{
		arr.push(Math.round(Math.random() * num));
	}
	return arr;
}
let num1 = +prompt("Размер");

//Напишите функцию, которая удаляет из массива повторяющиеся элементы и возвращает обновлённый массив.
function task6()
{
	arr = getRandomArray(num1);
	let k = 0;
	let result = [];
	for (let i = 0; i < arr.length; i++)
	{
		arr.sort((function(a, b) { return a - b; }));
		if(arr[i - 1] != arr[i])
		{
		  result.push(arr[i]);
		}

	}
	return {arr, result};
}
/*console.log(task6());*/

//Выведите в консоль элементы массива, которые больше среднего арифметического всех элементов.
function task8()
{
	arr = getRandomArray(num1);
	let sum = 0 , a;
	let result = [];
	for(let i = 0; i < arr.length; i++)
	{
		sum += arr[i];
		a = sum / arr.length; 
	}
	for ( let j = 0; j < arr.length; j++)
	{
		if (arr[j] > a)
			{
				result.push(arr[j]);
			}
	}
	return {arr, sum , a, result};
}
/*console.log(task8());*/

//Найдите два наименьших элемента массива.
function task8()
{
	arr = getRandomArray(num1);
	let min1 = arr[0], min2 = arr[0];
	for (let i = 0; i < arr.length; i++)
	{
		if (arr[i] <= min1)
		{
			min2 = min1;
			min1 = arr[i];
		}
		else if (arr[i] <= min2)
		{
			min2 = arr[i];
		}
	}

	return {arr, min1, min2};
}
/*console.log(task8());*/

//Удалите из массива все элементы, меньшие 0.3.
//Сдвиньте все оставшиеся элементы вперёд, а на освободившиеся места вставьте нули.
function task9()
{
	arr = getRandomArray(num1);
	let k = 0;
	for (let i = 0; i < arr.length; i++)
	{
		arr[i] /=20;
		
		if (arr[i] < 0.3)
		{
			k++;
		}
		else
		{
			arr[i-k] = arr[i];

		} 
	}
	for (let j = arr.length - k; j < arr.length; j++)
	{
		arr[j] = 0;
	}
	return arr;
}

/*console.log(task9());*/


//Попарно сложите элементы двух массивов равной длины: 
//первый массива 1 с последним массива 2, второй массива 1 
//с предпоследним массива 2 и так далее. Верните массив с результатами сложения.
function task10()
{
	let arr1 = getRandomArray(num1);
	let arr2 = getRandomArray(num1);
	let result = [], k = 0;

	for (let i = 0; i < arr1.length; i++)
	{
		for (let j = arr2.length - 1; j >= 0; j--) {
			result[k] = arr1[i] + arr2[j];
			k++;
			i++;
		}
	}

	return {arr1, arr2, result};
}

/*console.log(task10());*/
//Отсортируйте массив методом пузырька
function task11()
{
	arr = getRandomArray(num1);
	let num;
	for (let j = 0; j < arr.length; j++)
	{
		for (let i = 0; i < arr.length-1; i++)
		{
			if (arr[i] > arr[i+1])
			{
				num = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = num;
			}	
		}
	}
	return arr;
}
console.log(task11());
//!!!!Сделайте сдвиг массива вправо на X позиций (X передайте в функцию в качестве аргумента).
//Элементы, которые после сдвига "уходят" за пределы его длины, переместите на освободившиеся первые Х позиций.
function task14(k)
{
	arr = getRandomArray(num1);
	let result = [];
	let i;
	for (let i = 0; i < arr.length; i++)
	{
		result[i] = arr[i - k];
		for (let j = 0; j <  k; j++) 
		{
			result[j] = arr[i];
		}
	}
	return {arr, result};
}
/*console.log(task14(1));*/