/*1.Создайте функцию unique(arr), которая вернёт массив уникальных, не повторяющихся значений массива arr.*/
let values = [1,2,4,1,2,4];
function unique(arr) {
	let set = new Set(arr);
	for(let value of set)
	{
		console.log(value);
	}
}

/*unique(values);*/

/*2.Напишите функцию aclean(arr), которая возвращает массив слов, очищенный от анаграмм*/
let arr = ["nap", "teachers", "cheaters", "PAN", "ear", "era", "hectares"];

function aclean(arr)
{
	let map = new Map();
	let sort = [];
	for (let value of arr)
	{
		let text = value.toUpperCase().split('');
		sort = text.sort().join("");
		map.set(sort, value);
	}
	for (let result of map.values())
	{
		console.log(result)
	}
}

/*aclean(arr);*/


/*3.Пусть даны абзацы. Переберите эти абзацы циклом и создайте коллекцию Map, ключами в 
которой будут абзацы, а значением - их порядковый номер на странице. Сделайте так, 
чтобы по клику на любой абзац ему в конец записывался его порядковый номер.
*/
let p = document.querySelectorAll('p'); 
let map = new Map();
let arrP = [];
let arrtext = [];
for(let i = 0; i < p.length; i++)
{
	arrP.push(p[i]);
	arrtext[i]= document.getElementsByTagName("p")[i].textContent;
	map.set(arrtext[i], i);

}
	p.forEach(el => el.addEventListener("click", function() {
		let arr = [];
		arr += el.textContent;
		el.innerText += map.get(arr[0]);
}));
	/*console.log(map);*/



/*4.Сделайте функцию-генератор, которая будет возвращать итератор. Первый вызов метода next 
итератора должен вернуть 1, второй вызов - 2, третий вызов - 3.*/

function *task4()
{
	yield 1;
	yield 2;
	yield 3;
}

let iterator = task4();
for(let elem of iterator)
{
	console.log(elem);
}

/*console.log(iterator.next());
console.log(iterator.next());
console.log(iterator.next());*/

/*5.Дан массив с числами. С помощью функции-генератора создайте итератор, каждый вызов которого 
будет возвращать следующий элемент массива, являющийся четным числом.*/

let array = [1,2,3,4,5,6,7,8,9]

function *task5(array)
{
	for(let i = 0; i < array.length; i++)
	{
		if(array[i] % 2 == 0)
		{
			yield array[i];

		}

	}
}
let f = task5(array);
for(let elem of f)
{
	console.log(elem)
}


/*6.Дан массив с числами. С помощью функции-генератора создайте итератор, каждый вызов которого 
будет возвращать следующее число Фибоначчи.*/
function *fibonacci(array)
{
	let x = 0, y = 1;
	while (true)
	{
		yield y;
		[x,y] = [y, x + y];
	}
}

let fib = fibonacci(array);
for(let i = 0; i < array.length; i++)
{
	console.log(fib.next());
}