//Напишите функцию, которая получает три числа и возвращает их сумму.
function task1()
{
	let num1 = parseInt(prompt('Первое число', 10));
	while (!parseInt(num1))
	{
		num1 = parseInt(prompt('Первое число', 10));
	}
	let num2 = parseInt(prompt('Второе число', 5));
	while (!parseInt(num2))
	{
		num2 = parseInt(prompt('Второе число', 5));
	}

	let num3 = parseInt(prompt('Третье число', 13));
	while (!parseInt(num3))
	{
		num3 = parseInt(prompt('Третье число', 13));
	}

	return num1 + num2 + num3;
}  

/*console.log(task1());*/

//Напишите функцию, которая подсчитывает сумму чисел от 1 до заданного X.
function task2()
{
	let result = 0;
	let x = parseInt(prompt('Введите число ', 10));
	while (!parseInt(x))
	{
		x = parseInt(prompt('Введите число ', 10));
	}
	for (let i = 0; i <= x; i++)
	{
		result = result + i; 
	}

	return result;
}

/*console.log(task2());*/

//Напишите функцию, которая подсчитывает сумму цифр числа.
function task3()
{
	let x = prompt('Введите число ');
	while (!parseInt(x))
	{
		x = parseInt(prompt('Введите число ', 10));
	}
	let result = 0;

	for (let i = 0; i < x.length; i++)
	{
		result = result + parseInt(x[i]);
	}
	return result;
}

/*console.log(task3());*/

//Напишите функцию, которая считает факториал числа.
function task4(x)
{
	if (x == 0 )
	{
		return 1;
	}
	return task4(x - 1) * x;

}
/*console.log (task4(3));*/

//Создайте функцию, которая получает в качестве аргументов три числа и возвращает наименьшее из них.
function task5()
{
	let num1 = prompt('Первое число');
	while (!parseInt(num1))
	{
		num1 = prompt('Первое число');
	}
	let num2 = prompt('Второе число');
	while (!parseInt(num2))
	{
		num2 = prompt('Второе число');
	}
	let num3 = prompt('Третье число');
	while (!parseInt(num3))
	{
		num3 = prompt('Третье число');
	}

	let arr = [num1, num2, num3];
	let min = arr[0];
	for (let i = 0; i < arr.length; i++)
	{
		if (arr[i] <= min)
		{
			min = arr[i];
		}
	}
	
	return min;
}

/*console.log(task5());*/

// Создайте функцию, которая получает в качестве аргументов значения суток,
// часов и минут, а возвращает соответствующее им количество секунд.

function task6() 
{
	let day = prompt("Введите количество суток:");
	while (day < 0 || !parseInt(day) )
	{
	 day = prompt("Введите количество суток:");
	}
	let hour = prompt ("Введите количество часов:");
	while (hour < 0 || !parseInt(hour) || hour > 24)
	{
	 day = prompt("Введите количество часов:");
	}
	let minute = prompt ("Введите количество минут:");
	while (minute < 0 || !parseInt(minute) || minute > 60 )
	{
	 day = prompt("Введите количество минуты:");
	}

	return day * 86400 + hour * 3600 + minute * 60;
	
}
/*console.log(task6()); */

//Напишите рекурсивную функцию, которая разворачивает введённое число задом наперёд. 
//Например, если на вход функция получает 1234, вернуть она должна 4321.

function task7()
{
  let x = prompt("Введите число");
  while (!parseInt(x))
	{
		x = prompt('Введите число');
	}
  let result = [];

	for (let i = x.length-1; i >= 0; i--)
	{
		result += parseInt(x[i]);
	}
	return result;
	
}
/*console.log(task7());*/

/*Напишите функцию для решения квадратных уравнений. В качестве параметров она должна
 принимать коэффиценты a, b и c, возвращать – количество корней. Сами корни уравнения
 (или информацию о их отсутствии) функция должна выводить в консоль. Примечание 1.
 Ищем только решения в действительных числах, комплексные не нужны! Примечание 2.
 Для получения квадратного корня используйте стандартную функцию JS: Math.sqrt().*/

 function task8()
 {
 	let a = prompt("Введите а: ");
 	while (!parseInt(a))
	{
 		a = prompt("Введите а: ");
	}
 	let b = prompt("Введите b: ");
 	while (!parseInt(b))
	{
 		b = prompt("Введите b: ");
	}
 	let c = prompt("Введите c: ");
 	while (!parseInt(c))
	{
 		c = prompt("Введите c: ");
	}
 	let D = 0, x1 = 0, x2 = 0, x = 0;

 	D = b * b - 4 * a * c;

 	if (D < 0)
 	{
 		return("Дискриминант меньше нуля");
 	}
 	else if ( D == 0)
 	{
	 	x = (-b)/ (2 * a);
	 	return {x : x };
 	}
 	else
 	{
	 	x1 = (-b + Math.sqrt(D))/ (2 * a);
	 	x2 = (-b - Math.sqrt(D))/ (2 * a);

	 	return {x1 : +x1.toFixed(3), x2 : +x2.toFixed(3)};
 	}
 }

 console.log(task8());