/*1.Все элементы label внутри таблицы. Должно быть 3 элемента.*/
let a = document.getElementById('age-list');
//console.log(a);

/*2.Первую ячейку таблицы (со словом "Возраст").*/
let b = document.getElementsByTagName('td')[0];
//console.log(b);

/*3.Вторую форму в документе.*/
let c = document.getElementsByTagName('form')[1];
/*console.log(c);*/

/*4.Форму с именем search, без использования её позиции в документе.*/
let value = document.querySelector('form[name = "search"]');
/*console.log(value);*/

/*5.Элемент input в форме с именем search. Если их несколько, то нужен первый.*/
let value1 = document.querySelector('input[name = "search"]');
/*console.log(value1);*/

/*6.Элемент с именем info[0], без точного знания его позиции в документе.*/
let value2 = document.getElementsByName('info[0]');
/*console.log(value2);*/

/*7.Элемент с именем info[0], внутри формы с именем search-person.*/
let value3 = document.querySelector('form[name = "search-person"] [name = "info[0]"]');
/*console.log(value3);*/

/*8.Добавьте на страницу select с несколькими опциями, свяжите его с лейблом
и вставьте оба элемента перед кнопкой «Искать».*/

for (let i = 0; i < 4; i++)
{
	let select = document.createElement('select');
	let option = document.createElement('option');
	option.textContent = i;
	option.setAttribute('label', i)
	let value4 = document.querySelector('select');
	value4.appendChild(option);
	let input = document.querySelector('input[type = "submit"]');
    value4.insertAdjacentElement('afterEnd', input);
}


/*1.Дан элемент #elem. Добавьте ему класс "www".*/
let elem = document.getElementById('elem');
elem.classList.add('www');

/*2.Дан элемент #elem. Проверьте наличие у него класса "www", если есть — удалите этот класс.*/
let elem1 = document.getElementById('elem');
elem1.classList.contains('www');
elem1.classList.remove('www');

/*3.Дан ul. Дан массив. Вставьте элементы этого массива в конец ul так, чтобы каждый элемент стоял
в своем li. Сделайте так, чтобы четные позиции имели красный фон.*/

let arr = [4, 65, 4, 67];
let li, menu;
for(let i = 0; i < arr.length; i++)
{
	li = document.createElement('li');
	li.textContent = arr[i];
	menu = document.querySelector('ul');
	menu.appendChild(li);
	if (i % 2 == 0)
	li.style.color = 'red';
}

/*4.Дан элемент #elem. Найдите его соседа сверху и добавьте ему в начало и в конец по восклицательному знаку '!'*/
let elem3 = document.getElementById('elem');
let k = elem3.previousElementSibling;
k.append('!');
k.prepend('!');

/*5.!!!!Реализуйте функцию wrapInParagraph, которая находит каждый из дочерних текстовых узлов внутри 
элемента div и оборачивает их в элемент p.*/
function wrapInParagraph()
{
	let parent = document.getElementById('parent');
	let nodes = parent.childNodes;
	let node, result;
	let p = document.createElement('p');

	for (node of nodes) 
	{
		let p = document.createElement('p');
  		p.innerHTML = node.textContent;
  		node.replaceWith(p);
		console.log(node);
	}	
}
wrapInParagraph();


/*6.Реализуйте функцию normalizeClassNames, которая нормализует имена классов для всех элементов на
странице: все классы, которые содержат дефис (например, menu-main) должны быть заменены на классы
 в camelCase (menuMain).*/