/*1) Напишите функцию, которая создаёт и возвращает массив длиной 20 элементов, 
каждый из которых – случайное число от 1 до 50.*/
function task1()
{
	let arr = [];
	let num = 50;
	for (let i = 0; i < 20; i++)
	{
		arr.push(Math.round(Math.random() * num));

	}
	return arr;
}
/*console.log(task1());*/
/*2)Перепишите функцию из задачи 1, так, чтобы она принимала три аргумента: длину
нужного массива, минимальное и максимальное значения элементов.*/
function task2()
{
	let arr = [];
	let a = +prompt("Размер массива");
	let b = +prompt("Минимальное");
	let c = +prompt("Максимальное");

	function prog(a,b,c)
	{
		for (let i = 0; i < a; i++)
		{
			let num = Math.round(Math.random() * c);
			if (num > b)
			{
				arr.push(num);
			}
			else i--;
		}
		return arr;
	}
	let result = prog(a,b,c);
	return result;

}
/*console.log(task2());*/
/*3)Напишите функцию, которая проверяет, начинается ли строка на https:// 
и заканчивается ли она на .html. Если оба условия выполнены, 
функция возвращает true, в ином случае – false.*/
function task3()
{
	let str = prompt("Строка", "https://aaa.html");

	if (str.startsWith("https://") && str.endsWith(".html"))
	{
		return true;
	}
	else
		return false;
}
/*console.log(task3());*/
/*4) Напишите функцию, которая считает, сколько раз определённый символ встречается в строке.*/
function task4()
{
	let str = prompt("Строка");
	let symbol = prompt("Символ");
	let k = 0;

	for (let i = 0; i < str.length; i++)
	{
		if(str[i] == symbol)
		{
			k++;
	}	
	}
	return {str, k};
}
/*console.log(task4());*/

/*5)Перепишите функцию из задачи 4 так, чтобы она считала большие и маленькие буквы одним 
и тем же символом (например, 'A' == 'a').*/
function task5()
{
	let str = prompt("Строка");
	let symbol = prompt("Символ");
	let k = 0 , text;

	for(let i = 0; i < str.length; i++)
	{
		text = str.toUpperCase();
		s = symbol.toUpperCase();
		if (text[i] == s)
		{
			k++;
		}
		
	}
	return {str,  k};

}
/*console.log(task5());*/
/*6) Напишите функцию, которая выводит в консоль текущие дату, месяц и год в формате «24 января 2019».*/
function task6()
{
	let now = new Date();
	switch(now.getMonth()+1)
	{
		case 1:
		return console.log(now.getDate() + ' ' + "января" + ' ' + now.getFullYear());
		break;
		case 2:
		return console.log(now.getDate() + ' ' + "февраля" + ' ' + now.getFullYear());
		break;
		case 3:
		return console.log(now.getDate() + ' ' + "марта" + ' ' + now.getFullYear());
		break;
		case 4:
		return console.log(now.getDate() + ' ' + "апреля" + ' ' + now.getFullYear());
		break;
		case 5:
		return console.log(now.getDate() + ' ' + "мая" + ' ' + now.getFullYear());
		break;
		case 6:
		return console.log(now.getDate() + ' ' + "июня" + ' ' + now.getFullYear());
		break;
		case 7:
		return console.log(now.getDate() + ' ' + "июля" + ' ' + now.getFullYear());
		break;
		case 8:
		return console.log(now.getDate() + ' ' + "августа" + ' ' + now.getFullYear());
		break;
		case 9:
		return console.log(now.getDate() + ' ' + "сентября" + ' ' + now.getFullYear());
		break;
		case 10:
		return console.log(now.getDate() + ' ' + "октября" + ' ' + now.getFullYear());
		break;
		case 11:
		return console.log(now.getDate() + ' ' + "ноября" + ' ' + now.getFullYear());
		break;
		case 12:
		return console.log(now.getDate() + ' ' + "декабря" + ' ' + now.getFullYear());
		break;
	}
	return console.log(now.getDate() + ' ' + (now.getMonth()+1) + ' ' + now.getFullYear());
}
/*task6();*/
/*7) Напишите функцию, которая выводит в консоль количество секунд, прошедших с начала текущего дня.*/
function task7()
{
	let now = new Date();

	let sec = now.getSeconds();
	let min = now.getMinutes();
	let hour = now.getHours();

	let result = sec + min * 60 + hour * 3600;
	return {sec, min, hour, result};
}
/*console.log(task7());*/


/*8)Напишите функцию, которая принимает три числа, и определяет, можно ли построить треугольник со сторонами,
длина которых равна этим числам. Возвращать функция должна объект из двух свойств: possible (true или false)
и angles (объект с тремя свойствами, равными углам полученного треугольника в градусах).*/
function task8()
{
	let a = +prompt("Первая сторона");
	let b = +prompt("Вторая сторона");
	let c = +prompt("Третья сторона");

	function triangle(a, b, c)
	{

		let obj = 
		{
			possible: null,
		}
		let angles =
		{
			degrees1: null,
			degrees2: null,
			degrees3: null,
		}

		if (a < b + c && b < a + c && c < a + b)
		{
			obj.possible = true;
		}
		else 
		{
			obj.possible = false;
		}
		
		if (obj.possible = true)
		{
			angles.degrees1 = Math.cos(((Math.pow(b, 2) + Math.pow(c, 2) - Math.pow(a, 2)) / (2 * b * c)) * 180 / Math.PI);
			angles.degrees2 = Math.cos(((Math.pow(a, 2) + Math.pow(c, 2) - Math.pow(b, 2)) / (2 * a * c)) * 180 / Math.PI);
			angles.degrees3 = Math.cos(((Math.pow(a, 2) + Math.pow(b, 2) - Math.pow(c, 2)) / (2 * a * b)) * 180 / Math.PI);

		}
		let result = obj.possible +'\n'+ angles.degrees1 +'\n' + angles.degrees2 + '\n' + angles.degrees3;

		return result;
	}
	let num = triangle(a, b, c);
	return num;
}

console.log(task8());

/*9)Напишите функцию, которая определяет, является ли строка палиндромом. Учитывайте, что пробелы и знаки 
препинания не влияют на «палиндромность» строки!*/
function task9()
{
	let str = prompt("Строка");
	for(let i = 0; i < str.length/2; ++i)
    {
	    if(str[i] == str[str.length -  i - 1])
	    {
	      return true;
	    }
  }
  return false;
}
/*console.log(task9());*/


/*11)Напишите функцию, которая замяняет первую букву каждого слова в строке на такую же большую.*/
function task10()
{
	let str = prompt("Строка");
	let result = '';
	for (let i = 0; i < str.length; i++)
	{
		if (str[i - 1] == ' ' || i == 0 || str[i - 1] == '.') 
		{
      		result += str[i].toUpperCase();
   		} 
   		else 
   		{
     	    result += str[i];
   		}
	}
	return result;
}
/*console.log(task10());
*/

/*13)Напишите функцию, которая возвращает текущий день недели на русском языке.*/
function task11()
{
	let now = new Date();
	switch(now.getDay())
	{
		case 0:
		return console.log("Воскресенье");
		break;
		case 1:
		return console.log("Понедельник");
		break;
		case 2:
		return console.log("Вторник");
		break;
		case 3:
		return console.log("Среда");
		break;
		case 4:
		return console.log("Четверг");
		break;
		case 5:
		return console.log("Пятница");
		break;
		case 6:
		return console.log("Суббота");
		break;

	}
}
/*task11();*/
