/*1.Создайте функцию sumArgs(), которая будет суммировать все свои аргументы.*/
function sumArgs() {
	let arr = [].slice.call(arguments);
	let result = 0;
	for (let i = 0; i < arr.length; i++)
	{
		result += arr[i];
	}

	console.log(result);	
}
sumArgs(1,2,4,5,6);

/*2.Найдите два наименьших элемента массива*/
function minMas()
{
	let result;
	arguments.slice = [].slice.call(arguments);
	let min1 = arguments[0], min2 = arguments[0];
	for (let i = 0; i < arguments.length; i++)
	{
		if (arguments[i] <= min1)
		{
			min2 = min1;
			min1 = arguments[i];
		}
		else if (arguments[i] <= min2)
		{
			min2 =  arguments[i];
		}

	}
	return {min1, min2};

}
console.log(minMas(6,33,55,5,13,10));

/*3.Создайте объект "Цилиндр" (свойства – радиус и высота). Добавьте в него метод, который считает объём цилиндра*/

function value(V, mistake)
{
	if (cylinder.h > 0 && cylinder.r > 0)
	{
		V();
	}
	else
		mistake();
}
let cylinder  = {
	name: "Цилиндр",
	h: prompt('Высота'),
	r: prompt('Радиус'),
	V()
	{
		alert(Math.PI * this.r * this.r * this.h);
	},
	mistake()
	{
		alert('Введите положительные числа.');

	},
};
value(cylinder.V.bind(cylinder), cylinder.mistake.bind(cylinder));


/*4.Сделайте функцию, которая параметрами принимает 2 числа. 
Если их сумма больше 10 - пусть функция вернет true, а если нет - false.*/
let result = (a, b) => (a + b) > 10 ? true : false;
/*console.log(result(5,13));*/

/*5.Дан массив. Запишите первый элемент этого массива в переменную elem1, 
второй - в переменную elem2, третий - в переменную elem3, а все остальные
элементы массива - в переменную arr.*/
function task5()
{
	let array = [1,3,4,5,7,8];
	let [elem1, elem2, elem3, ...arr] = array;
	return {elem1, elem2, elem3, arr};
}
/*console.log(task5());*/

/*6.Дан массив. Запишите последний элемент этого массива в переменную elem1, а предпоследний - в переменную elem2.*/
function task6()
{
	let array = [1,2,3,4,5,6,7,8];
	let arr = array.reverse();
	let [elem1, elem2] = arr;
	return {elem1, elem2};
}
/*console.log(task6());*/

/*7.Дан объект {name: 'Петр', 'surname': 'Петров', 'age': '20 лет'}. 
Запишите соответствующие значения в переменные name, surname и age.*/
function task7()
{
	let obj = {
		name: 'Петр', 
		surname: 'Петров', 
		age: '20 лет'	
	};
	let {name, surname, age} = obj;
	console.log('Name '+ name + 'Surname '+ surname + 'Age ' + age);
}
/*task7();*/

/*8.Дан массив, выведите его элементы последовательно на экран в обратном порядке*/
function task8()
{
	let arr = [1,2,3,4,5,6,7,8];
	let array = arr.reverse();
	for(let result of array)
	{
		console.log(result);
	}
}
/*task8();*/

/*9.Дана строка. С помощью for-of подсчитайте количество букв 'о' в ней.*/
function task9()
{
	let k = 0;
	let str = 'Дана строка. С помощью for-of подсчитайте количество букв о в ней';
	for (let result of str)
	{
		if (result == 'о')
		{
			k++;
		}
	}
	console.log(k);
}
/*task9();*/
/*10.Даны абзацы с числами. Сделайте так, чтобы по нажатию на абзац начинал тикать таймер, 
который каждую секунду будет увеличивать на единицу число в этом абзаце. По нажатию на
другой абзац должно происходить то же самое - он тоже начнет тикать.*/
	let p = document.querySelectorAll('p');
	let m;
	p.forEach(function(item)
	{
		item.addEventListener('click', function f()
			{
				function fun()
				{
					item.innerHTML = parseInt(item.innerHTML) + 1;
				}
				setInterval(fun, 1000);
			});
	});

